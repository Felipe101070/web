package ltpWeb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/CicloDeVida")
public class CicloDeVida extends HttpServlet{		
	private int contador;

	@Override
	public void init() throws ServletException {
		System.out.println("Servlet Iniciado.");
		System.out.println("Contador inicial: " + contador);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		PrintWriter out = resp.getWriter();
		HttpSession session = req.getSession();
		contador = contador + 1;
	
		req.setAttribute("contador", contador);
		RequestDispatcher dispatcher = req.getRequestDispatcher("JCicloDeVida.jsp");
		dispatcher.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException{
		PrintWriter out = resp.getWriter();
		HttpSession session = req.getSession();
		session.setMaxInactiveInterval(10);
		
		contador = contador + 1;
		out.println("<html>");
		out.println("<head>");
		out.println("</head>");
		out.println("<body>");
		if(req.getParameter("Senha0").equals(req.getParameter("Senha1"))) {
			out.println("<h1>Seja bem vindo!, " + req.getParameter("Login") + "</h1>");
			out.println("<h3>Id sess�o: "+ session.getId() + "</h3>");
			out.println("<h3>Id sess�o: "+ session.getMaxInactiveInterval() + "</h3>");
		}else {
			out.println("<h2>Acesso de n�mero: " + contador + "</h2>");
			out.println("<a href=\"rediext\"> Usuario n�o autenticado! </a>");
		}
		out.println("</body>");
		out.println("</html>");
	}
	@Override
	public void destroy() {
		System.out.println("Servlet destruido.");
		System.out.println("Contador final: " + contador);
	}
}
